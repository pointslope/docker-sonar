FROM pointslope/openjdk7:0.4.0

MAINTAINER Point Slope "developers@pointslope.com"


ENV SONAR_JDBC_URL "jdbc:mysql://$SONAR_DB_PORT_3306_TCP_ADDR:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true"
ENV SONAR_USER "sonar"
ENV SONAR_PASSWORD "REPLACE ME"
ENV SONAR_LOG_LEVEL DEBUG

RUN echo "deb http://downloads.sourceforge.net/project/sonar-pkg/deb binary/" > /etc/apt/sources.list.d/sonar.list && DEBIAN_FRONTEND=noninteractive apt-get update -q && apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes sonar \
 && rm -rf /var/lib/apt/lists/*

COPY conf/wrapper.conf /opt/sonar/conf/wrapper.conf
COPY conf/sonar.properties /opt/sonar/conf/sonar.properties

# Install Sonar plugins
ADD plugins.sh /tmp/plugins.sh
WORKDIR /opt/sonar/extensions/downloads
RUN /tmp/plugins.sh
WORKDIR /opt/sonar

EXPOSE 9000

CMD ["bin/linux-x86-64/sonar.sh", "console"]
